<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <meta name="description" content="PHP domaci - funkcije i varijabli.">
        <meta name="keywords" content="php, functions, variables">
        <meta name="author" content="Atila Alacan">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>PHP domaci - funkcije i varijabli</title>
        <link href="https://fonts.googleapis.com/css?family=Fira+Mono|Fira+Sans" rel="stylesheet">
        <link rel="stylesheet" href="css/normalize.css">
        <link rel="stylesheet" href="css/styles.css">
    </head>
    <body>
        <nav></nav>
        <main>
            <h2>Upload Your Image</h2>
            <p class="code--int"><?php if (isset($_GET['error'])) { echo '* All fields are required!'; } ?></p>
            <div class="form">
                <form action="myPhoto.php" method="post" enctype="multipart/form-data">
                <lable>Name</lable><br>
                <input type="text" name="name" value="<?php if (isset($_GET['name'])) { echo $_GET['name']; } ?>" required>
                <span class="code--int"><?php if (isset($_GET['nameErr'])) { echo 'Field missing!'; } ?></span><br><br>
                <label>Select Image</label><br />
                <input type="file" name="file" id="if"/><br /><br />
                <input type="radio" name="privacy" value="private" checked> Private<br>
                <input type="radio" name="privacy" value="public"> Public<br><br /><br />
                <input type="submit" name="sub" id="sub" value="Upload" />
                </form>
            </div>
        </main>
    </body>
</html>