# Domaći zadatak

* Napraviti stranicu koja ima obrazac za upload JPG slike
* Pored polja za odabir slike kreirati polje za unose imena korisnika kao i dva radio tastera sa vrednošću private i public
* Stranica šalje podatke na myPhoto.php stranicu koja proverava da li su sva polja ispunjena i da li je slika tipa jpg (za proveru koristiti biblioteku EXIF)
* Ukoliko je sve u redu slika se postavlja u direktorijum public ili private (u zavisnosti od odabira)
* Ime slike se menja prema šablonu: ime korisnika-trenutni vremenski žig-random broj između 1 i 10.jpg (Npr: test- 1517167681-4.jpg)